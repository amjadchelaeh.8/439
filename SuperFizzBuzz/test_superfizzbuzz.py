import imp
from SuperFizzBuzz.superfizzbuzz import mod_superfizzbuzz

import unittest


class superfizzbuzzTest(unittest.TestCase):
    def test_give_3_is_Fizz(self):
        num = 3
        is_Fizz = mod_superfizzbuzz.Fizz(num)
        self.assertEqual(is_Fizz, "Fizz")   

    def test_give_5_is_Buzz(self):
        num = 5
        is_Fizz = mod_superfizzbuzz.Buzz(num)
        self.assertEqual(is_Fizz, "Buzz")   

    def test_give_15_is_FizzBuzz(self):
        num = 15
        is_Fizz = mod_superfizzbuzz.FizzBuzz(num)
        self.assertEqual(is_Fizz, "FizzBuzz") 

    def test_give_9_is_FizzFizz(self):
        num = 9
        is_Fizz = mod_superfizzbuzz.FizzFizz(num)
        self.assertEqual(is_Fizz, "FizzFizz")   

    def test_give_25_is_BuzzBuzz(self):
        num = 25
        is_Fizz = mod_superfizzbuzz.BuzzBuzz(num)
        self.assertEqual(is_Fizz, "BuzzBuzz")   

    def test_give_225_is_FizzFizzBuzzBuzz(self):
        num = 225
        is_Fizz = mod_superfizzbuzz.FizzFizzBuzzBuzz(num)
        self.assertEqual(is_Fizz, "FizzFizzBuzzBuzz")     

    def test_give_11111_is_NoFizzBuzz(self):
        num = 11111
        is_Fizz = mod_superfizzbuzz.NoFizzBuzz(num)
        self.assertEqual(is_Fizz, "NoFizzBuzz")