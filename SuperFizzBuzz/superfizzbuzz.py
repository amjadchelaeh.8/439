class mod_superfizzbuzz:

    def Fizz(num):
        if num % 3 == 0:
            return "Fizz"
    
    def Buzz(num):
        if num % 5 == 0:
            return "Buzz"
    
    def FizzBuzz(num):
        if num % 3 == 0 and num % 5 == 0:
            return "FizzBuzz"

    def FizzFizz(num):
        if num % 9 == 0:
            return "FizzFizz"

    def BuzzBuzz(num):
        if num % 25 == 0:
            return "BuzzBuzz"

    def FizzFizzBuzzBuzz(num):
        if num % 9 == 0 and num % 25 == 0:
            return "FizzFizzBuzzBuzz"
    
    def NoFizzBuzz(num):
        if num <= 0  or num > 10000:
            return "NoFizzBuzz"


##A = mod_superfizzbuzz
##for i in range (1,10):
    ##print(A.Fizz(i))